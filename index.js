// TODO: 1.Numbers and Date
/*Numbers*/
//You can use four types of number literals: decimal, binary, octal, and hexadecimal
let decimal = 0777;
let binary = 0b10000000000000000000000000000000;
let octal = 0755;
let hexadecimal = 0xfffffffffffffffff;

console.log(decimal);
console.log(binary);
console.log(octal);
console.log(hexadecimal);

/*Number object*/
let num = Number.MAX_VALUE;
num = Number.MIN_VALUE;
num = Number.NaN;
num = Number.POSITIVE_INFINITY;
num = Number.NEGATIVE_INFINITY;
num = Number.parseFloat('21');
num = Number.parseInt('55.6');
num = Number.isNaN(2);

console.log(num);

/*Math object*/
let num2 = Math.PI;
num2 = Math.SQRT2;
num2 = Math.abs(-4);
num2 = Math.floor(-5.05);
num2 = Math.random();
num2 = Math.round(1.5);

console.log(num2);

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

console.log(getRandomInt(3));

/*Date object*/
let now = new Date();
console.log(now);

function JSClock() {
  let time = new Date();
  let hour = time.getHours();
  let minute = time.getMinutes();
  let second = time.getSeconds();
  let temp = '' + (hour > 12 ? hour - 12 : hour);
  if (hour == 0) temp = '12';
  temp += (minute < 10 ? ':0' : ':') + minute;
  temp += (second < 10 ? ':0' : ':') + second;
  temp += hour >= 12 ? ' P.M.' : ' A.M.';
  return temp;
}
console.log(JSClock());

// TODO: 2.Text formatting
let str = 'This is a string with copy right logo\xA9';

console.log(str);

/*String objects*/
let strObj = 'This is String for tutorial string obj    ';

console.log(strObj.length);
console.log(strObj[3]);
console.log(strObj.indexOf('t'));
console.log(strObj.split(' '));
console.log(strObj.slice(0 /*Start*/, 4 /*End*/));
console.log(strObj.toLowerCase());
console.log(strObj.toUpperCase());
console.log(strObj.trim());

/*template literals*/
let strTemp =
  'we can\'t write "string" text in one line \n\
and we should use template literals';

console.log(strTemp);

let tnx = 'thanks';
let oh = 'Oh';
console.log(`${oh}, ${tnx} god for ES6`);

// TODO: 3.Indexed collections
let number = new Array(1, 2, 3, 4, 5);
console.log(number);

let hero = ['Batman', 'SuperMan', 'WonderWoman'];

console.log(hero[0]);
console.log(hero.length);
hero[3] = 'Flash';
console.log(hero);
console.log(hero.length);
console.log(hero.push('IronMan'));
// console.log(hero.pop());
console.log(hero.unshift('SpiderMan'));
// console.log(hero.shift());
console.log(hero);
console.log(hero.slice(1 /*start_index*/, 4 /*upto_index*/));
console.log(hero.join(' - '));
console.log(hero.sort());

//map
hero.map((item) => console.log(item));

const products = ['Book', 90, true];
//foreach
products.forEach((item) => console.log(item));
//filter
products.filter((item) => {
  return typeof item === number;
});
console.log(products);
console.log(products.indexOf('Book'));

const [title, price] = products;
console.log(title);
console.log(price);
